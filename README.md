# Aplicaciones en HTML
## HTML5 + CSS3 + ES6
Pruebas:
- [Chrome](https://www.google.com/intl/es_es/chrome/)
- [Firefox](https://www.mozilla.org/es-ES/firefox/new/)
- Edge (integrado en Windows)
___
Intención didáctica:
- Nombres explicativos.
- Código claro y con comentarios.
- Toda la aplicación en el mismo archivo para maximizar la portabilidad y lectura.
- Imágenes en base64 (más rápido y portable que en un archivo aparte).
- Uso [emoticonos](https://www.google.com/search?q=emoji) para evitar imágenes.
___ 
[caniuse](https://caniuse.com/)  
- Si está implementado en los navegadores actuales lo uso.  
- Sin preocupaciones de retrocompatibilidad.
___

Marco Antonio Arias Antolín. 2020  
<dervinas7@gmail.com>